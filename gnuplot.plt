set term cairolatex pdf
set out 'fig3.tex'
set samp 1000
set xrange [-4:4]
set key off
set label 1 '\huge$\frac{1}{\sqrt{2\pi}\sigma}\,e^{-\frac{x^2}{2\sigma^2}}$' at -3.5,.34
set label 2 '\Large$\sigma = 1$' at 0.95,.3
set label 3 '\Large$\sigma = 2$' at 2.7,.1
plot for [s=1:2] exp(-x**2/(2*s**2))/(s*sqrt(2*pi)) lw 3
set out
