# LaTex-gnuplot
An example of how to use gnuplot to create figures and then embedd them into LaTeX. Doing it this way will make the text of the axis, titles and labels copy-and-pastable and will render the text with the same font as the rest of the latex-document.

* Generate the random data [rn.dat](rn.dat) with the python script [randomData.py](randomData.py)
* Set up your plot with gnuplot [gnuplot.plt](gnuplot.plt)
* Create the LaTeX-file [main.tex](main.tex) and input the figure like this: `\input[file3]`
* Compile your document and have a look at the beautiful figure you have created
* ???
* Profit


![Screenshot of the output](screenshot.png)
