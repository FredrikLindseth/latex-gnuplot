from numpy import arange, random, exp
f = open('rn.dat', 'w')
a = 2
b = 3
for v in arange(-2, 2, 1./250):
  f.write("%g %g\n" % (v, a*exp(-b*v**2) + .15*random.normal()))
f.close()
